<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'api'], function () use ($router) {

    // To auth
    $router->post('auth/login', 'AuthController@login');
    $router->post('auth/register', 'AuthController@register');
    $router->post('auth/refresh', 'AuthController@refresh');

    // Logged in
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('users/self', 'UserController@self');
        $router->get('users/{id}', 'UserController@show');
        $router->put('users/self', 'UserController@update');
        $router->post('auth/logout', 'AuthController@logout');
        $router->get('interests', 'InterestController@index');
        $router->post('interests', 'InterestController@create');
        $router->delete('interests/{interest_id}', 'InterestController@delete');
        $router->get('interests/predictions', 'InterestController@predictions');
        $router->get('matches', 'MatchController@index');
        $router->get('matches/likedMe', 'MatchController@likedMe');
        $router->post('matches/like', 'MatchController@like');
        $router->post('matches/dontlike', 'MatchController@dontlike');
        $router->get('matches/likes', 'MatchController@getLikes');
        $router->get('tags/user/{user_id}', 'TagController@getByUser');
        $router->post('tags/{tag_id}/user/{user_id}', 'TagController@tag');
        $router->delete('tags/user/{user_id}', 'TagController@untag');
    });
    
    // Categories
    $router->get('categories', 'CategoryController@index');
    $router->get('categories/find/{name}', 'CategoryController@findByName');

    // Countries
    $router->get('countries', 'CountryController@index');

    // Coins
    $router->put('users/{username}/reward/{coins}', 'UserController@reward');
    $router->put('users/self/spend/{coins}', 'UserController@spend', ['middleware', 'auth']);

    // Temporary
    $router->delete('users/{instagram_id}', 'UserController@delete');
    $router->get('tags', 'TagController@index');
    $router->post('tags', 'TagController@create');
    $router->put('tags/{id}', 'TagController@update');
});