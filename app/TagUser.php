<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class TagUser extends Pivot
{

    protected $table = 'tags_users';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    public function from()
    {
        return $this->belongsTo('App\User', 'from_id')
        ->select('id', 'username', 'full_name', 'birthdate', 'profile_picture');
    }
}
