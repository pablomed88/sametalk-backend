<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;

class Match extends Model
{
    /**
     * Status
     */
    const PENDING = 0;
    const ACCEPTED = 1;
    const REJECTED = 2;

    protected $statuses = [
        'pending' => 0,
        'accepted' => 1,
        'rejected' => 2
    ];

    /**
     * Type
     */
    const LIKE = 0;
    const SUPERLIKE = 1;

    protected $types = [
        'like' => 0,
        'superlike' => 1,
    ];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['user'];

    // Mutators

    // Accessors
    public function getStatusAttribute($status)
    {
        return array_search($status, $this->statuses);
    }

    public function getTypeAttribute($type)
    {
        return array_search($type, $this->types);
    }

    public function getUserAttribute()
    {
        if ($this->attributes['to_id'] == Auth::id()) {
            return User::with('country')->find($this->attributes['from_id']);
        } else {
            return User::with('country')->find($this->attributes['to_id']);
        }
    }

    // Relations
    public function from()
    {
        return $this->belongsTo('App\User', 'from_id');
    }
    
    public function to()
    {
        return $this->belongsTo('App\User', 'to_id');
    }

}
