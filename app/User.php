<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Carbon\Carbon;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'instagram_id', 'username', 'full_name', 'profile_picture',
        'bio', 'follows', 'followed_by', 'birthdate', 'gender',
        'country_id', 'coins', 'player_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $appends = [
        'age'
    ];

    
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    // Mutators
    // public function setAgeAttribute($age)
    // {
    //     $year = Carbon::now()->year - $age;
    //     $this->attributes['age'] = new Carbon($year.'-01-01');
    // }

    public function setCountryIdAttribute($code)
    {
        $country = Country::where('code', $code)->first();
        $this->attributes['country_id'] = $country->id;
    }
    
    // Accessors
    public function getAgeAttribute($age)
    {
        return Carbon::now()->diffInYears($this->attributes['birthdate']);
    }

    // Relations
    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function interests()
    {
        return $this->hasMany('App\Interest')->with('category');
    }

    public function matches()
    {
        return $this->hasMany('App\Match', 'from_id')->with('to');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'tags_users', 'to_id', 'tag_id')
        ->using('App\TagUser')
        ->withPivot('from_id');
    }
}
