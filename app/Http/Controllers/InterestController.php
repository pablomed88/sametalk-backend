<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Interest;
use App\User;
use App\Category;
use Illuminate\Database\QueryException;
use Exception;

class InterestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        return User::find(Auth::id())->interests;
    }

    public function create(Request $request)
    {
        foreach($request->all() as $interest) {
            $cat = Category::with('parent')->find($interest['cat_id']);
            $interest['cat1_id'] = $cat->parent->parent->id;
            $interest['cat2_id'] = $cat->parent->id;
            $interest['cat3_id'] = $cat->id;
            $interest['user_id'] = Auth::id();
            try {
                Interest::create($interest);
            } catch(QueryException $ex) {
                return response()->json([
                    'status' => 'error',
                    'message' => $ex->getMessage()
                ], 400);
            }
        }
        return response()->json([
            'status' => 'ok',
            'message' => 'Successfully created.'
        ]);
    }
    
    public function delete($cat_id)
    {
        $int = Interest::whereCat3Id($cat_id)->whereUserId(Auth::id())->first();
        try {
            if ($int !== null) {
                $int->delete();
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Interest doesnt exists'
                ], 400);
            }
        } catch(QueryException $ex) {
            return response()->json([
                'status' => 'error',
                'message' => $ex->getMessage()
            ], 400);
        }
        return response()->json([
            'status' => 'ok',
            'message' => 'Successfully deleted.'
        ]);        
    }

    public function predictions(Request $request)
    {
        $id = Auth::id();
        $predictions = DB::select('select l2.user_id, sum(
            case 
                when (l1.cat2_id = l2.cat2_id and l1.cat3_id = l2.cat3_id) then 1
                when (l1.cat2_id = l2.cat2_id) then 0.25 
                else 0.05 
            end) as score
            from interests l1
            inner join interests l2 on l1.cat1_id = l2.cat1_id and l2.user_id <> ?
            left join matches m on m.from_id = ? and m.to_id = l2.user_id
            left join matches m2 on m2.from_id = l2.user_id and m2.to_id = ?
            where l1.user_id = ? and m.id is null and (m2.status=0 or m2.id is null)
            group by l2.user_id
            order by score desc', [$id, $id, $id, $id]);
        $users = collect();
        $interestsQty = User::withCount('interests')->find($id)->interests_count;
        foreach($predictions as $prediction) {
            $user = User::with('country')->find($prediction->user_id);
            $user->score = $prediction->score;
            $score = ($prediction->score > $interestsQty) ? $interestsQty : $prediction->score;
            $user->compatibility = round(($score / $interestsQty) * 100);
            $user->compatibility .= '%';
            $users->add($user);
        }

        // Filters
        if ($request->has('age_more_than')) {
            $users = $users->filter(function($item) use($request) {
                return $item->age >= $request->age_more_than;
            });
        }

        if ($request->has('age_less_than')) {
            $users = $users->filter(function($item) use ($request) {
                return $item->age <= $request->age_less_than;
            });
        }

        if ($request->has('gender')) {
            $users = $users->where('gender', strtoupper($request->gender));
        }

        if ($request->has('country')) {
            $users = $users->filter(function ($user) use ($request) {
                return $user->country->code == strtoupper($request->country);
            });
        }

        return response()->json([
				'status' => 'ok',
				'message' => $users->values(),
        ]);
    }
}
