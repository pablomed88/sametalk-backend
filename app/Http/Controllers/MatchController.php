<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Match;
use Illuminate\Database\QueryException;
use OneSignal;

class MatchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        return Match::whereStatus(Match::ACCEPTED)
        ->where(function($q) {
            $q->where(function($query) {
                $query->whereFromId(Auth::id());
            })
            ->orWhere(function($query) {
                $query->whereToId(Auth::id());
            });
        })
        ->get();
    }

    public function likedMe()
    {
        return Match::whereStatus(Match::PENDING)
        ->whereToId(Auth::id())
        ->get();
    }

    public function like(Request $request)
    {
        $match = Match::whereFromIdAndToId($request->to_id, Auth::id())->first();
        if ($match) {
            $match->status = Match::ACCEPTED;
            //Send notification to $request->to_id
            $user = User::find($request->to_id);
            if ($user->player_id) {
                $message = "¡Tienes un nuevo match con " . Auth::user()->full_name . "!";
                $parameters = [
                    "contents" => [
                        "en" => $message, 
                        "es" => $message
                    ],
                    "include_player_ids" => [$user->player_id],
                ];
                OneSignal::sendNotificationCustom($parameters);
            }
        } else {
            $match = Match::whereFromIdAndToId(Auth::id(), $request->to_id)->first();
            if ($match) {
                $match->status = Match::PENDING;
            } else {
                $match = new Match;
                $match->to_id = $request->to_id;
                $match->from_id = Auth::id();
                $match->status = Match::PENDING;
                $match->type = Match::LIKE;
            }
            if ($request->type == 'superlike') {
                $match->type = Match::SUPERLIKE;
                //Send notification to $request->to_id
                $user = User::find($request->to_id);
                if ($user->player_id) {
                    $message = "¡" . Auth::user()->full_name . " te dio un Superlike!";
                    $parameters = [
                        "contents" => [
                            "en" => $message, 
                            "es" => $message
                        ],
                        "include_player_ids" => [$user->player_id],
                    ];
                    OneSignal::sendNotificationCustom($parameters);
                }
            }
        }
        try {
            $match->save();
        } catch(QueryException $ex) {
            return response()->json([
                'status' => 'error',
                'message' => $ex->getMessage()
            ], 400);
        }
        return response()->json([
            'status' => 'ok',
            'message' => 'Successfully created/updated.',
            'match' => $match
        ]);  
    }
    
    public function dontlike(Request $request)
    {
        $match = Match::whereFromIdAndToId($request->to_id, Auth::id())->first();
        if ($match) {
            $match->status = Match::REJECTED;
        } else {
            $match = Match::whereFromIdAndToId(Auth::id(), $request->to_id)->first();
            if ($match) {
                $match->status = Match::REJECTED;
            } else {
                $match = new Match;
                $match->to_id = $request->to_id;
                $match->from_id = Auth::id();
                $match->status = Match::REJECTED;
                $match->type = Match::LIKE;
            }
        }
        try {
            $match->save();
        } catch(QueryException $ex) {
            return response()->json([
                'status' => 'error',
                'message' => $ex->getMessage()
            ], 400);
        }
        return response()->json([
            'status' => 'ok',
            'message' => 'Successfully created/updated.'
        ]);    
    }

    public function getLikes()
    {
        return Match::whereStatus(Match::PENDING)
        ->whereToId(Auth::id())
        ->get();
    }

}
