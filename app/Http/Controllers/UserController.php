<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        return User::all();
    }

    public function self()
    {
        return User::with('country')->find(Auth::id());
    }

    public function show($id)
    {
        return User::find($id);
    }

    public function delete($instagram_id)
    {
        $user = User::where('instagram_id', $instagram_id)->first();
        $user->delete();
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $user->fill($request->all());
        $user->save();
        return User::with('country')->find($user->id);
    }

    public function reward($username, $coins)
    {
        $user = User::where('username', $username)->first();
        if ($user) {
            $user->coins = $user->coins + $coins;
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'User not found'
            ], 404);
        }
        $user->save();
        return $user;
    }

    public function spend($coins)
    {
        $user = Auth::user();
        if ($user->coins - $coins >= 0) {
            $user->coins = $user->coins - $coins;
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Insufficient coins'
            ], 400);
        }
        $user->save();
        return $user;
    }
}
