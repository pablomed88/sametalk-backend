<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Tag;
use App\TagUser;
use App\User;
use Illuminate\Database\QueryException;

class TagController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        return Tag::all();
    }

    public function create(Request $request)
    {
        return Tag::create($request->all());
    }
    
    public function update(Request $request, $tagId)
    {
        $tag = Tag::find($tagId);
        $tag->fill($request->all());
        return $tag->save();
    }

    public function getByUser($user_id)
    {
        $user = User::find($user_id);
        $tags = $user->tags->makeHidden('pivot');
        $groupedTags = array();
        foreach ($tags as $tag) {
            $tagId = $tag['id'];
            if (!isset($groupedTags[$tagId])) {
                $groupedTags[$tagId] = $tag->toArray();
                $groupedTags[$tagId]['count'] = 0;
                $groupedTags[$tagId]['users'] = array();
            }
            $groupedTags[$tagId]['count'] += 1;
            array_push($groupedTags[$tagId]['users'], $tag->pivot->from);
        }
        return collect($groupedTags)->values();
    }

    public function tag($tagId, $userId)
    {
        $tagUser = TagUser::whereFromId(Auth::id())->whereToId($userId)->first();
        if ($tagUser) {
            $tagUser->tag_id = $tagId;
            $tagUser->save();
        } else {
            $user = User::find($userId);
            $user->tags()->attach($tagId, ['from_id' => Auth::id()]);
        }
    }

    public function untag($userId)
    {
        $user = User::find($userId);
        $user->tags()->wherePivot('from_id', Auth::id())->detach();        
    }

}
