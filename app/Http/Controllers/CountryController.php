<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\Country;

class CountryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        return Country::all();
    }

}
