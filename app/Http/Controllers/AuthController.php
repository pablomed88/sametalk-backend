<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Database\QueryException;

class AuthController extends Controller
{

    public function login(Request $request)
    {
        $this->validate($request, [
            'instagram_id' => 'required',
        ]);
        $user = User::where('instagram_id', $request->instagram_id)->first();
        if (!$user || !$token=Auth::login($user)) {
            return response()->json([
				'status' => 'error',
				'message' => 'Invalid credentials.',
			], 401);
        }
		return response()->json([
			'status' => 'ok',
			'token' => $token,
		]);        
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'instagram_id' => 'required|unique:users',
            'username' => 'required|unique:users',
        ]);
        try {
            $user = User::create($request->all());
        } catch(QueryException $ex){ 
            return response()->json([
                'status' => 'error',
                'message' => $ex->getMessage()
            ], 400);
        }
        $user = User::with('country')->find($user->id);
        $user->token = Auth::login($user);
        return $user;
    }

	public function logout(Request $request) {
		try {
            Auth::invalidate($request->bearerToken);
			return response()->json([
				'status' => 'ok',
				'message' => 'Successfully logged out.'
			]);
		} catch (JWTException $exception) {
			return response()->json([
				'status' => 'error',
				'message' => 'Cannot logout.'
			], 500);
		}
	}

    public function refresh(Request $request)
    {
        if ($new_token=Auth::refresh()) {
            return $new_token;
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Invalid credentials.',
            ], 401);
        }
    }
}
