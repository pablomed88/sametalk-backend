<?php

namespace App\Http\Controllers;
use App\Category;
use App\User;
use Auth;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $categories = Category::with('children')->where('parent_id', null)->get();
        $id = Auth::id();
        if ($id) {
            $interests = User::find($id)->interests->pluck('category.id')->toArray();
            foreach ($categories as $category) {
                foreach ($category->children as $subCategory) {
                    foreach ($subCategory->children as $subSub) {
                        $subSub->selected = in_array($subSub->id, $interests) ? true : false;
                    }
                }
            }
            return $categories; // Return categories with "selected" attribute
        } else {
            return $categories; // Return categories
        }
    }

    public function show($id)
    {
        return Category::find($id);
    }

    public function findByName($str)
    {
        return Category::where('name', 'like', '%'.$str.'%')->get();
    }
}
