<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'image', 'parent_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['parent_id'];

    // Mutators
    public function getImageAttribute($image)
    {
        return 'data:image/jpeg;base64,' . base64_encode($image);
    }

    public function parent()
    {
        return $this->belongsTo('App\Category', 'parent_id')->with('parent');
    }
    
    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id')->with('children');
    }
}
