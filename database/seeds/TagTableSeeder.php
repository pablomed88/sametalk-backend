<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/tags.json');
        $data = json_decode($json);
        foreach ($data as $key => $row) {
            $image = str_replace('data:image/png;base64,', '', $row->image);
            $image = str_replace(' ', '+', $image);
            DB::table('tags')->insert([
                'name' => $key,
                'image' => base64_decode($image)
            ]); 
        }
    }
}
