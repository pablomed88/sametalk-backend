<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country_id = DB::table('countries')->where('code', 'AR')->value('id');
        DB::table('users')->insert([
            "instagram_id" => "15609529387",
            "username" => "nicomontoya___",
            "profile_picture" => "https://scontent.cdninstagram.com/vp/562a8805bb9feb13306233f2c118d525/5E29B1F1/t51.2885-19/s150x150/65860960_2438899326344281_5296198106264633344_n.jpg?_nc_ht=scontent.cdninstagram.com",
            "full_name" => "Nico Montoya",
            "bio" => "",
            "follows" => 661,
            "followed_by" => 729,
            "birthdate" => "1995-01-01",
            "gender" => "M",
            "country_id" => $country_id,
            "coins" => 0,
            "created_at" => "2019-10-21",
            "updated_at" => "2019-10-21"
        ]);
    }
}
