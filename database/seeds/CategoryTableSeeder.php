<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/categories.json');
        $data = json_decode($json);
        foreach ($data as $key => $row) {
            $image = str_replace('data:image/jpeg;base64,', '', $row->image);
            $image = str_replace(' ', '+', $image);
            $parent = DB::table('categories')->insertGetId([
                'name' => $key,
                'image' => base64_decode($image),
                'parent_id' => null
            ]);
            foreach ($row->categories as $key2 => $row2) {
                $image2 = str_replace('data:image/jpeg;base64,', '', $row2->image);
                $image2 = str_replace(' ', '+', $image2);
                $parent2 = DB::table('categories')->insertGetId([
                    'name' => $key2,
                    'image' => base64_decode($image2),
                    'parent_id' => $parent
                ]);
                foreach ($row2->categories as $key3 => $row3) {
                    $image3 = str_replace('data:image/jpeg;base64,', '', $row3->image);
                    $image3 = str_replace(' ', '+', $image3);
                    DB::table('categories')->insert([
                        'name' => $key3,
                        'image' => base64_decode($image3),
                        'parent_id' => $parent2
                    ]);    
                }
            }
        }
        // // Parents
        // $deportes = DB::table('categories')->insertGetId([
        //     'name' => 'Deportes',
        //     'parent_id' => null,
        // ]);
        // $politica = DB::table('categories')->insertGetId([
        //     'name' => 'Política',
        //     'parent_id' => null,
        // ]);
        // $paises = DB::table('categories')->insertGetId([
        //     'name' => 'Países',
        //     'parent_id' => null,
        // ]);

        // // Subcategories
        // $futbol = DB::table('categories')->insertGetId([
        //     'name' => 'Futbol',
        //     'parent_id' => $deportes,
        // ]);
        // $tenis = DB::table('categories')->insertGetId([
        //     'name' => 'Tenis',
        //     'parent_id' => $deportes,
        // ]);
        // $ucr = DB::table('categories')->insertGetId([
        //     'name' => 'Unión Cívica Radical',
        //     'parent_id' => $politica,
        // ]);
        // $peronismo = DB::table('categories')->insertGetId([
        //     'name' => 'Peronismo',
        //     'parent_id' => $politica,
        // ]);
        // $kirchnerismo = DB::table('categories')->insertGetId([
        //     'name' => 'Kirchnerismo',
        //     'parent_id' => $politica,
        // ]);
        // $argentina = DB::table('categories')->insertGetId([
        //     'name' => 'Argentina',
        //     'parent_id' => $paises,
        // ]);
        // $españa = DB::table('categories')->insertGetId([
        //     'name' => 'España',
        //     'parent_id' => $paises,
        // ]);
        // $brasil = DB::table('categories')->insertGetId([
        //     'name' => 'Brasil',
        //     'parent_id' => $paises,
        // ]);

        // // Deportes
        // DB::table('categories')->insert([
        //     'name' => 'River Plate',
        //     'parent_id' => $futbol,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Boca',
        //     'parent_id' => $futbol,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Independiente',
        //     'parent_id' => $futbol,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Racing',
        //     'parent_id' => $futbol,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'San Lorenzo',
        //     'parent_id' => $futbol,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Barcelona FC',
        //     'parent_id' => $futbol,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Real Madrid',
        //     'parent_id' => $futbol,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Estudiantes de La Plata',
        //     'parent_id' => $futbol,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'JM Del Potro',
        //     'parent_id' => $tenis,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Rafael Nadal',
        //     'parent_id' => $tenis,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Roger Federer',
        //     'parent_id' => $tenis,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Andy Murray',
        //     'parent_id' => $tenis,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Serena Williams',
        //     'parent_id' => $tenis,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Novak Djokovic',
        //     'parent_id' => $tenis,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Diego Schwartzman',
        //     'parent_id' => $tenis,
        // ]);

        // // Politica
        // DB::table('categories')->insert([
        //     'name' => 'Raul Alfonsín',
        //     'parent_id' => $ucr,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Frondizi',
        //     'parent_id' => $ucr,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Juan Domingo Perón',
        //     'parent_id' => $peronismo,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Evita',
        //     'parent_id' => $peronismo,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Nestor Kirchner',
        //     'parent_id' => $kirchnerismo,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Cristina Fernandez de Kirchner',
        //     'parent_id' => $kirchnerismo,
        // ]);

        // // Países
        // DB::table('categories')->insert([
        //     'name' => 'Asado',
        //     'parent_id' => $argentina,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Mate',
        //     'parent_id' => $argentina,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Paella',
        //     'parent_id' => $españa,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Flamenco',
        //     'parent_id' => $españa,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Portugués',
        //     'parent_id' => $brasil,
        // ]);
        // DB::table('categories')->insert([
        //     'name' => 'Carnavales',
        //     'parent_id' => $brasil,
        // ]);
    }
}
