<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/countries.json');
        $data = json_decode($json);
        $i = 1;
        foreach ($data as $key => $row) {
            $image = str_replace('data:image/png;base64,', '', $row->flag);
            $image = str_replace(' ', '+', $image);
            try {
                DB::table('countries')->insert([
                    'code' => $key,
                    'name' => $row->name->common,
                    'flag' => base64_decode($image)
                ]);
            } catch(\Illuminate\Database\QueryException $ex){
                dd($ex->getMessage());
            }
            $i++;
        }
    }
}
