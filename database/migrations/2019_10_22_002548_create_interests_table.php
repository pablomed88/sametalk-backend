<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInterestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('cat1_id');
            $table->unsignedBigInteger('cat2_id');
            $table->unsignedBigInteger('cat3_id');
            $table->timestamps();
            $table->unique(['user_id', 'cat1_id', 'cat2_id', 'cat3_id']);
        });
        Schema::table('interests', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('cat1_id')->references('id')->on('categories');
            $table->foreign('cat2_id')->references('id')->on('categories');
            $table->foreign('cat3_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interests');
    }
}
