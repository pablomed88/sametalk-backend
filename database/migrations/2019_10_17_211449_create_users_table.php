<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('instagram_id', 20);
            $table->string('username', 30);
            $table->string('full_name', 45);
            $table->string('profile_picture', 300)->default("");
            $table->string('bio', 150)->default("");
            $table->integer('follows')->default(0);
            $table->integer('followed_by')->default(0);
            $table->date('birthdate');
            $table->enum('gender', ['M', 'F', 'O']);
            $table->unsignedBigInteger('country_id');
            $table->integer('coins')->default(0);
            $table->string('player_id', 36)->nullable();
            $table->timestamps();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
